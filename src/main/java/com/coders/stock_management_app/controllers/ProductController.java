package com.coders.stock_management_app.controllers;

import java.util.List;
import java.util.stream.Stream;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.coders.stock_management_app.models.Product;
import com.coders.stock_management_app.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;



@RestController
@RequestMapping("/produts")

public class ProductController {
    @Autowired
    private ProductService productService;
    @PostMapping
    public ResponseEntity<?> saveProducts(@RequestBody @Valid Product product,BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            Stream<String> stringStream = fieldErrors.stream()
                    .map(fieldError -> fieldError.getDefaultMessage() + "::");
            String error = stringStream.findFirst().get();
            return ResponseEntity.badRequest().body(error);
        }
        return ResponseEntity.ok(productService.createProduct(product));
    }
    @GetMapping
    public List<Product> getProducts() {
        return productService.getProducts();
    }
    @DeleteMapping("/{productId}")
    public ResponseEntity<?> deleteProduct(@PathVariable long productId){
        productService.deleteById(productId);
        return ResponseEntity.ok("Succefuly deleted!");
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getAllbyId(@PathVariable Long id){
        return ResponseEntity.ok(productService.findById(id));
    }
    
    }