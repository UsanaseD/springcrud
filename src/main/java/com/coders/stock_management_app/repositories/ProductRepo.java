package com.coders.stock_management_app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.coders.stock_management_app.models.Product;
import java.util.List;

@Repository
public interface ProductRepo extends JpaRepository<Product,Long>{
 List<Product> findAllById (Long id);
}