package com.coders.stock_management_app.services;

import java.util.List;

import com.coders.stock_management_app.models.Product;
import com.coders.stock_management_app.repositories.ProductRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
@Autowired
private ProductRepo productRepo;

public Product createProduct(Product product){
    return productRepo.save(product);
}

public List<Product> getProducts(){
    return productRepo.findAll();
}
public List<Product> getProduct(Long id){
    return productRepo.findAllById(id);
}
public void deleteById(Long id) {
 productRepo.deleteById(id);
}

public Object findById(Long id) {
	return productRepo.findById(id);
}

}
