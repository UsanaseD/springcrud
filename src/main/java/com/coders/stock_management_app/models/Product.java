package com.coders.stock_management_app.models;

import lombok.Data;

import java.sql.Date;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Min;
import javax.validation.constraints.PositiveOrZero;
import org.hibernate.annotations.CreationTimestamp;

@Entity

@Data
public class Product{
@Id
@GeneratedValue
private long id;
@Min(value=100, message="price can't go below 100")
private int price ;
@NotBlank(message = "description can't be blank")
private String description;
@NotBlank(message = "name can't be blank")
private String name;
@PositiveOrZero(message = "quantity cant be a negative number")
private int quantity;
@CreationTimestamp
private Date date;
}
