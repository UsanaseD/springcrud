## Table of Contents

* [Project Overview](#Project-Overview)
* [Features](#Features)
* [Built with](#built-with)
* [API End Points](#API-End-Points)
* [Known Issues](#Known-issues)
* [Installation](#Installation)
* [Contributing](#contributing)


## Project Overview

**Stock management App** A small App that helps a Store Manager to manage the store Items `spring-boot`

## Features

- A manager can add new product,
- A manager can modify a product,
- A manager can remove a product,
- A manager can view all products,
- A manager can view one product,
- A manager can add  product items,
- A manager can add  product items,


## Built with
- `Java`
- `Spring-boot`



## API End Points
- `POST /Products`                -   adds a new product


## Known issues
Everything works as expected; However:
- Did only one endpoint cause of machine disurbance and planning to end all of them by the end of 21st feb 2019

## Installation

- $ git clone `git@gitlab.com:UsanaseD/springcrud.git`
- $ cd springcrud
- $ ./mvnw spring-boot:run

## Contributing
>  Feel free to 🍴 fork this repository

>  👯 Clone this repository to your local machine using `https://gitlab.com/UsanaseD/springcrud.git`

> Make Contributions

> 🔃 Create a new pull request using `https://gitlab.com/UsanaseD/springcrud/compare`

- Copyright 2020 © DreamTeam_Coders
